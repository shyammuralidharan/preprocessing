import json
import numpy as np
import pandas as pd
from keras.models import model_from_json
from keras.preprocessing import sequence
#import itertools

class SpamHamModel():
    def __init__(self):
        self.config = self.read_file('./Model/youtube_model/Model_Parameters_Exec/spamfilter_twitter_parameters.json')
        pass

    @staticmethod
    def read_file(config_path):
        with open(config_path,'r') as f:
            return json.load(f)

    def predictSpam(self,text):
        # load model parameters
        # j = self.get_config('Model_Parameters/spamfilter_twitter_parameters.json', 'r')
        # loaded_model_parameters = json_file.read()
        # json_file.close()
        # j = json.loads(loaded_model_parameters)

        # read from saved dictionary
        # with open(self.config['path']['dictionary'], 'r') as dictionary_file:
        #     vocabulary = json.load(dictionary_file)
        vocabulary_tf_idf= self.read_file(self.config['path']['dictionary'])

        # read saved model structure
        json_file = open(self.config['path']['model'], 'r')
        loaded_model_json = json_file.read()
        json_file.close()

        # and create a model from that3e4srd5 bhnfg
        model = model_from_json(loaded_model_json)

        # weight the nodes with saved values
        weights = self.config['path']['weights']
        model.load_weights(weights)

        # for human-friendly printing
        labels = [self.config['label']['1'], self.config['label']['2']]
        maxlen = self.config['max_len']


        data={'Contents':[text]}
        print(data)
        temp_df=pd.DataFrame.from_dict(data)
        print(temp_df)
        input= temp_df['Contents'].apply(lambda x: x.split(' ')).tolist()
        print(input)
        input = [['this', 'is', 'my', 'content']]
        print(input)
        # TF-IDF data
        output_tf_idf = np.array([[vocabulary_tf_idf.get(word, 0) for word in sentence] for sentence in input])
        print(output_tf_idf)
        output_tf_idf = sequence.pad_sequences(output_tf_idf, maxlen=maxlen)
        print(output_tf_idf)
        # predict which bucket your input belongs in
        pred_tf_idf = model.predict(output_tf_idf)
        print(pred_tf_idf)
        return labels[np.argmax(pred_tf_idf)]

obj = SpamHamModel()

print(obj.predictSpam("this is a text"))
# df = pd.read_csv("Model/data/traindata.csv", encoding='latin-1')
# df_li=[]
# li=[]
# spm = []
#
# for i in range(50):
#     print("Iteration {}".format(i))
#     df_li.append(df["Contents"][i])
#     li.append(obj.predictSpam(df.Contents[i]))
#     spm.append(df["Spamflag"][i])
# print(li)
# pdf1=pd.DataFrame({"Spam Flag":li,"Actual_data":df_li,"Spam_Actual":spm})
# pdf1.to_csv("spam_output.csv")
#
