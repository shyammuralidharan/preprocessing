import json
import numpy as np
import pandas as pd
from keras.models import model_from_json
from keras.preprocessing import sequence

def model_load(text):

    #### Data Pre processing ###
    data = {'Contents': [text]}
    temp_df = pd.DataFrame.from_dict(data)
    input = temp_df['Contents'].apply(lambda x: x.split(' ')).tolist()

    ### YouTube Comments Model ###
    ytc_json = open(r'./Model/ytc_model/Model_Parameters_Exec/spamfilter_twitter_parameters.json', 'r')
    ytc_model_parameters = ytc_json.read()
    ytc_json.close()
    ytc = json.loads(ytc_model_parameters)
    with open(ytc['path']['dictionary'], 'r') as dictionary_file:
        ytc_vocabulary = json.load(dictionary_file)
    ytc_maxlen =ytc['max_len']
    # read saved model structure
    ytc_json = open(ytc['path']['model'], 'r')
    loaded_model_json = ytc_json.read()
    ytc_json.close()
    # and create a model from that
    ytc_model = model_from_json(loaded_model_json)
    # weight the nodes with saved values
    ytc_model.load_weights(ytc['path']['weights'])

    ytc_input = np.array([[ytc_vocabulary.get(word, 0) for word in sentence] for sentence in input])
    ytc_input = sequence.pad_sequences(ytc_output, maxlen=ytc_maxlen)

    ytc_pred = ytc_model.predict(ytc_output)

    ### YouTube Model ###
    yt_json = open(r'./Model/youtube_model/Model_Parameters_Exec/spamfilter_twitter_parameters.json', 'r')
    yt_model_parameters = yt_json.read()
    yt_json.close()
    yt = json.loads(yt_model_parameters)
    with open(yt['path']['dictionary'], 'r') as dictionary_file:
        yt_vocabulary = json.load(dictionary_file)
    yt_maxlen =yt['max_len']
    # read saved model structure
    yt_json = open(yt['path']['model'], 'r')
    loaded_model_json = yt_json.read()
    yt_json.close()
    # and create a model from that
    yt_model = model_from_json(loaded_model_json)
    # weight the nodes with saved values
    yt_model.load_weights(yt['path']['weights'])

    yt_input = temp_df['Contents'].apply(lambda x: x.split(' ')).tolist()
    yt_input = np.array([[yt_vocabulary.get(word, 0) for word in sentence] for sentence in input])
    yt_input = sequence.pad_sequences(yt_output, maxlen=yt_maxlen)

    yt_pred = yt_model.predict(yt_input)

    ### Twitter Model ###
    tw_json = open('./Model/twitter_model/Model_Parameters_Exec/spamfilter_twitter_parameters.json', 'r')
    tw_model_parameters = tw_json.read()
    tw_json.close()
    tw = json.loads(tw_model_parameters)
    with open(tw['path']['dictionary'], 'r') as dictionary_file:
        tw_vocabulary = json.load(dictionary_file)
    tw_maxlen = tw['max_len']
    # read saved model structure
    tw_json = open(tw['path']['model'], 'r')
    loaded_model_json = tw_json.read()
    tw_json.close()
    # and create a model from that
    tw_model = model_from_json(loaded_model_json)
    # weight the nodes with saved values
    tw_model.load_weights(tw['path']['weights'])

    tw_input = np.array([[tw_vocabulary.get(word, 0) for word in sentence] for sentence in input])
    tw_input = sequence.pad_sequences(tw_output, maxlen=tw_maxlen)

    tw_pred = tw_model.predict(tw_input)



    ### Blogs and Forums Model ###
    bnf_json = open('./Model/bnf_model/Model_Parameters_Exec/spamfilter_twitter_parameters.json', 'r')
    bnf_model_parameters = bnf_json.read()
    bnf_json.close()
    bnf = json.loads(bnf_model_parameters)
    with open(bnf['path']['dictionary'], 'r') as dictionary_file:
        bnf_vocabulary = json.load(dictionary_file)
    bnf_maxlen = bnf['max_len']
    # read saved model structure
    bnf_json = open(bnf['path']['model'], 'r')
    loaded_model_json = bnf_json.read()
    bnf_json.close()
    # and create a model from that
    bnf_model = model_from_json(loaded_model_json)
    # weight the nodes with saved values
    bnf_model.load_weights(bnf['path']['weights'])

    bnf_input = np.array([[vocabulary_tf_idf.get(word, 0) for word in sentence] for sentence in input])
    bnf_input = sequence.pad_sequences(bnf_output, maxlen=bnf_maxlen)

    return ytc_model,yt_model, tw_model, bnf_model


print(model_load("This is an input"))