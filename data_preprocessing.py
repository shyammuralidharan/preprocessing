from html_parser import HTMLParser
from basic_cleaning import BasicCleaning
from lemmatizer import Lemmatizer
#from spam_classification import SpamHamModel
import pandas as pd
import time
import spacy
#import json
from nltk.corpus import stopwords
import csv

class DataPreprocess():

    def __init__(self):
        self.nlp = spacy.load('en_core_web_sm')
        self.stopwords = stopwords.words('english')

    def main(self,text,sr):
        ob_html_parser = HTMLParser()
        ob_basic_clean=BasicCleaning()
        ob_lemmatizer=Lemmatizer()
        #ob_spam = SpamHamModel()
        response_htmlparser = ob_html_parser.process(text)
        response_basic =ob_basic_clean.process(response_htmlparser,self.stopwords)
        response_lemma=ob_lemmatizer.process(response_basic,self.nlp)
        final_cleaned = ob_basic_clean.process(response_lemma,self.stopwords)
        # if (sr=="Twitter"):
        #     spam_ham = ob_spam.predictSpam(final_cleaned)
        # else:
        #     spam_ham="NA"
        dict_preprocess={}
        dict_preprocess["Raw_Content"]=text
        dict_preprocess["Modified_Content"] = final_cleaned
        dict_preprocess["Source"] =sr
        #dict_preprocess["Spam"] = spam_ham
        return dict_preprocess


data_preprocess = DataPreprocess()
df = pd.read_csv("./Model/twitter_model/data/traindata.csv", encoding='latin-1')
df.head()
org_li=[]
mod_li=[]
src = []
spam = []

# print(data_preprocess.main("this is an , input! texxxxxtt sooooo much , WoowwwwwWWW!!!! @username #hashtag","Twitter"))
start_time = time.time()
for i in range(len(df)):
    print("Iteration {}".format(i))
    org_li = []
    mod_li = []
    src = []
    spam = []
    data = []
    out=[]
    with open("temp.csv","a") as f:
        writer = csv.writer(f)
        t=data_preprocess.main(df.Contents[i],"Twitter")
        #org_li.append(df.Contents[i])
        #data.append(org_li)
        mod_li.append(t["Modified_Content"])
        data.append(mod_li)
        src.append(t["Source"])
        #spam.append(df.Spamflag[i])
        #data.append(spam)
        out.append(data)
        writer.writerow(out)
        #data.append(org_li,mod_li,src,spam)
        f.close()

end_time = time.time()
total_time = end_time-start_time

# pdf1=pd.DataFrame({"orginal_cont":org_li,"mod_cont":mod_li,"Source":src,"Spam":spam})
# pdf1.to_csv("test_4.csv",encoding='latin-1')

print("Total time in seconds: %s",total_time)
print("Total Time in Minutes {}".format((total_time)/60))
#print("Total time in minutes: %m",total_time)
