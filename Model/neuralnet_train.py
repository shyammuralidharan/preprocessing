# -*- coding: ISO-8859-1 -*-
"""
Created on Wed Jan 10 11:35:16 2018

@author: gunjari.Bhattacharya
"""

from __future__ import print_function

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Embedding, Conv1D, Convolution1D,MaxPooling1D, LSTM
from keras.layers import Flatten
from keras import regularizers
from Model.datahelper import load_train_data, load_test_data
import json
from numpy.random import seed


seed(1)
########   LOAD ALL CONFIGURATIONS   #########
# load model parameters
json_file = open('./bnf_model/Model_Parameters/spamfilter_twitter_parameters.json')
loaded_model_parameters = json_file.read()
json_file.close()
j = json.loads(loaded_model_parameters)

max_features = j['max_features']
maxlen = j['max_len']  # cut texts after this number of words (among top max_features most common words)
batch_size = j['batch_size']

print('Loading data...')
x_train, y_train, vocabulary, vocabulary_inv, df, labels,label_dict = load_train_data(filename=j['path']['traindata'], vocab_limit= max_features)
x_test, y_test = load_test_data(filename=j['path']['testdata'], vocabulary = vocabulary)
#x_test, y_test = load_test_data(filename=j['path']['testdata'])


with open(j['path']['dictionary'], 'w') as dictionary_file:
    json.dump(vocabulary, dictionary_file)
if max_features>len(vocabulary):
    dict_len = len(vocabulary)
else:
    dict_len = max_features
print("Legnth of Vocabulary: {}, Length` of Dictionary {}".format(len(vocabulary),dict_len));
print(len(x_train), 'train sequences')
#print(len(x_test), 'test sequences')

print('Pad sequences (samples x time)')
x_train = sequence.pad_sequences(x_train, maxlen=maxlen)
x_test = sequence.pad_sequences(x_test, maxlen=maxlen)
print('x_train shape:', x_train.shape)

print('Build model...')
#model = Sequential()
#model.add(Embedding(max_features, 128))
#model.add(Embedding(maxlen, 50))
#model.add(LSTM(40))
#model.add(Dense(2, activation='sigmoid'))

# Building the model structure
model = Sequential()
model.add(Embedding(input_dim=dict_len+1, output_dim=128, input_length=maxlen))
model.add(LSTM(100))
model.add(Dense(2, activation='sigmoid'))

#model.add(Conv1D(128,5,padding='valid',activation='relu',strides=1))
#model.add(MaxPooling1D(5))
#model.add(LSTM(100))
#model.add(Flatten())
#model.add(Dropout(0.05))
#model.add(Dense(80, activation='relu'))
#model.add(Dropout(0.3))
#model.add(Dense(100, activation='relu'))
#model.add(Dropout(0.2))
#model.add(Dense(256, activation='tanh',kernel_regularizer=regularizers.l2(0.00001),activity_regularizer=regularizers.l1(0.00001)))

model.add(Dense(2, activation='tanh'))
model.add(Dense(2, activation='softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

model.summary();

print('Train...')
model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=j['epochs'],
          validation_split=j['validation_split'])

model_json = model.to_json()
with open(j['path']['model'], 'w') as json_file:
    json_file.write(model_json)

model.save_weights(j['path']['weights'])
print('Test...')

loss, acc = model.evaluate(x_test,y_test,
                            batch_size=batch_size)
print('Loss:', loss)
print('Test Accuracy:', acc)