# -*- coding: utf-8 -*-
"""
Created on Wed Jan 10 11:35:16 2018

@author: shyam.Muralidharan
"""
import json
import statistics as sd
import numpy as np
import keras
import pandas as pd
import keras.preprocessing.text as kpt
from keras.preprocessing.text import Tokenizer
from keras.models import model_from_json
from datahelper import load_real_data, load_test_data
from keras.preprocessing import sequence
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

import sys


########   LOAD ALL CONFIGURATIONS   #########
# load model parameters
json_file = open('./youtube_model/Model_Parameters/spamfilter_twitter_parameters.json', 'r')
loaded_model_parameters = json_file.read()
json_file.close()
j = json.loads(loaded_model_parameters)

# for human-friendly printing
labels = [j['label']['1'],j['label']['2']]
max_features = j['max_features']
maxlen = j['max_len']

# read in our saved dictionary
with open(j['path']['dictionary'], 'r') as dictionary_file:
    dictionary = json.load(dictionary_file)
    
# read in your saved model structure
json_file = open(j['path']['model'], 'r')
loaded_model_json = json_file.read()
json_file.close()
# and create a model from that
model = model_from_json(loaded_model_json)
# and weight your nodes with your saved values
model.load_weights(j['path']['weights'])

test_df=pd.read_csv(filepath_or_buffer=j['path']['testdata'],sep=",",encoding="ISO-8859-1",usecols=['Contents','Spamflag'])
#test_df=test_df[test_df['Spamflag']=='Ham']
print(len(test_df))

x_test, y_test = load_test_data(filename=j['path']['testdata'],vocabulary = dictionary);
x_test = sequence.pad_sequences(x_test, maxlen=maxlen)
    
# predict which bucket your input belongs in
pred = model.predict(x_test)
print(pred)

predicted=[]
 
for p in pred:
    predicted.append(labels[np.argmax(p)])
print(len(predicted))

test_df['Prediction']=predicted
arraysum=sum(test_df['Spamflag']==test_df['Prediction'])
accuracy=arraysum/len(test_df['Spamflag']) * 100
print(test_df[0:15])
test_df.to_csv(j['path']['resultdata'], sep=',', encoding = "ISO-8859-1")                     
print("%s Accuracy:" %accuracy)

y_pred=predicted
y_true=test_df['Spamflag']
print(confusion_matrix(y_pred,y_true))

print(classification_report(y_pred,y_true))

orig_stdout = sys.stdout
f = open('modelsummary.txt', 'w')
sys.stdout = f
print(model.summary())
print("Confusion matrix")
print("")
print(confusion_matrix(y_pred,y_true))
print("")
print("Classification Report")
print(classification_report(y_pred,y_true))

print("Number of records: ")
print(len(test_df))

sys.stdout = orig_stdout
f.close()



