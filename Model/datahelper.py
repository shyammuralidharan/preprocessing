# -*- coding: ISO-8859-1 -*-
"""
Created on Thu Dec 21 12:49:52 2017

@author: varun.saptharishi
"""

import re
import logging
import itertools
import numpy as np
import pandas as pd
#import gensim as gs
from collections import Counter
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.tokenize import word_tokenize

logging.getLogger().setLevel(logging.INFO)

def load_embeddings(vocabulary):
    word_embeddings = {}
    for word in vocabulary:
        word_embeddings[word] = np.random.uniform(-0.25, 0.25, 300)
    return word_embeddings

def pad_sentences(sentences, padding_word="<PAD/>", forced_sequence_length=None):
    """Pad setences during training or prediction"""
    if forced_sequence_length is None: # Train
        sequence_length = max(len(x) for x in sentences)
    else: # Prediction
        logging.critical('This is prediction, reading the trained sequence length')
        sequence_length = forced_sequence_length
    logging.critical('The maximum length is {}'.format(sequence_length))

    padded_sentences = []
    for i in range(len(sentences)):
        sentence = sentences[i]
        num_padding = sequence_length - len(sentence)

        if num_padding < 0: # Prediction: cut off the sentence if it is longer than the sequence length
#            logging.info('This sentence has to be cut off because it is longer than trained sequence length')
            padded_sentence = sentence[0:sequence_length]
        else:
            padded_sentence = sentence + [padding_word] * num_padding
        padded_sentences.append(padded_sentence)
    return padded_sentences

def build_vocab(sentences, limit):
    word_counts = Counter(itertools.chain(*sentences))
    vocabulary_inv = ["<PAD/>"] + [word[0] for word in word_counts.most_common(limit)];
    vocabulary = {word: index for index, word in enumerate(vocabulary_inv)}
    return vocabulary, vocabulary_inv

def build_vocab_Tf_Idf(text,vocab_limit):
    vectorizer = TfidfVectorizer(max_features=vocab_limit)
    vector = vectorizer.fit_transform(text)  ## Even astype(str) would work
    d = dict(zip(vectorizer.get_feature_names(), vector.data))
    d1 = sorted(d.items(), key=lambda x: x[1],reverse=True)
    vocabulary_inv = ["<PAD/>"] + [word[0] for word in d1]
    vocabulary = {word: index for index, word in enumerate(vocabulary_inv)}
    return vocabulary, vocabulary_inv


def batch_iter(data, batch_size, num_epochs, shuffle=True):
    data = np.array(data)
    data_size = len(data)
    num_batches_per_epoch = int(data_size / batch_size) + 1

    for epoch in range(num_epochs):
        if shuffle:
            shuffle_indices = np.random.permutation(np.arange(data_size))
            shuffled_data = data[shuffle_indices]
        else:
            shuffled_data = data

        for batch_num in range(num_batches_per_epoch):
            start_index = batch_num * batch_size
            end_index = min((batch_num + 1) * batch_size, data_size)
            yield shuffled_data[start_index:end_index]

def load_train_data(filename, vocab_limit):
    df_train = pd.read_csv(filename, encoding = "ISO-8859-1")

    selected = ['Spamflag', 'Contents']
    non_selected = list(set(df_train.columns) - set(selected))

    df_train = df_train.drop(non_selected, axis=1)
#    df = df.dropna(axis=0, how='any', subset=selected)
    df_train = df_train.reindex(np.random.permutation(df_train.index)) #random shuffling

#creating a dictionary with the one-hot encoding of the labels ("Ham","Spam")
    labels = sorted(list(set(df_train[selected[0]].tolist()))) #convert to set to ignore duplicates
    num_labels = len(labels)
    one_hot = np.zeros((num_labels, num_labels), int)
    np.fill_diagonal(one_hot, 1)
    label_dict = dict(zip(labels, one_hot));print("Train data Labels:",label_dict)

    tf_idf_input = df_train[selected[1]]
#    print(x_raw)
    # x_raw is a list of individual words after cleaning the strings
    y_raw = df_train[selected[0]].apply(lambda y: label_dict[y]).tolist()
#    x_raw = pad_sentences(x_raw, forced_sequence_length = 40)
    vocabulary, vocabulary_inv = build_vocab_Tf_Idf(tf_idf_input,vocab_limit)

    x_raw = df_train[selected[1]].apply(lambda x: x.split(' ')).tolist()
    x = np.array([[vocabulary.get(word,0) for word in sentence] for sentence in x_raw])
#    print(x)
    y = np.array(y_raw)
#    print(y)
    return x, y, vocabulary, vocabulary_inv, df_train, labels,label_dict

#def load_real_data(filename, vocabulary):
def load_real_data(df, vocabulary):
#    df = pd.read_csv(filename)  commented by varun since utf-8 CODEC is not sufficient
#    df = pd.read_csv(filename, encoding = "ISO-8859-1", keep_default_na=None, error_bad_lines = False) 
#    selected = ['Category', 'Descript']
#    columns = ['No', 'Source', 'Host', 'Link', 'Date(ET)', 'Time(ET)', 'LocalTime',
#      'Category', 'Author ID', 'Author Name', 'Author URL', 'Authority',
#      'Followers', 'Following', 'Age', 'Gender', 'Language', 'Country',
#      'Province/State', 'City', 'Location', 'Sentiment', 'Alexa Rank',
#      'Alexa Reach', 'Title', 'Snippet', 'Contents', 'Summary', 'Bio',
#      'Unique ID']
#    
#    df.columns = columns
    selected = ['Contents']
    non_selected = list(set(df.columns) - set(selected))

    df = df.drop(non_selected, axis=1)
    
    x_raw= df[selected[0]].apply(lambda x: x.split(' ')).tolist()
#    x_raw = pad_sentences(x_raw)

    x = np.array([[vocabulary.get(word,0) for word in sentence] for sentence in x_raw])
    return x

def load_test_data(filename, vocabulary):
#    df = pd.read_csv(filename)  commented by varun since utf-8 CODEC is not sufficient
    df = pd.read_csv(filename, encoding = "ISO-8859-1") 
    
    selected = ['Spamflag', 'Contents']
    non_selected = list(set(df.columns) - set(selected))

    df = df.drop(non_selected, axis=1)

    labels = sorted(list(set(df[selected[0]].tolist())))
    num_labels = len(labels)
    one_hot = np.zeros((num_labels, num_labels), int)
    np.fill_diagonal(one_hot, 1)
    label_dict = dict(zip(labels, one_hot));print("Test data Labels:",label_dict)
    
    x_raw= df[selected[1]].apply(lambda x: x.split(' ')).tolist()
    y_raw = df[selected[0]].apply(lambda y: label_dict[y]).tolist()

    x = np.array([[vocabulary.get(word,0) for word in sentence] for sentence in x_raw])
    y = np.array(y_raw)
    return x, y

if __name__ == "__main__":
    train_file = './data/traindata.csv'