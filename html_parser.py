from bs4 import BeautifulSoup

class HTMLParser():

    def __init__(self):
        pass

    def process(self, text):
        #print("tag removal started")
        try:
            soup = BeautifulSoup(text, "lxml")
            # kill all script and style elements
            for script in soup(["script", "style"]):
                script.decompose()  # rip it out
            # get text
            text = soup.get_text()
            txt = text.encode('ascii', 'replace').decode()
            # txt = text
        except (TypeError):
            txt = text
        #print("tag removal finished")
        return txt

