import re
import itertools
#from nltk.corpus import stopwords


class BasicCleaning():
    def __init__(self):
        pass

    def process(self, text,stopwords):

        # takes the input for cleaning
        stop = stopwords

        text1 = ""
        try:
            text1 = str(text)  # converted into string format
            text1 = re.sub(r'\b[\w\-.]+?@\w+?\.\w{2,4}\b', '', text1)  # email addresses handled
            text1 = re.sub(r'(http[s]?\S+)|(\w+\.[A-Za-z]{2,4}\S*)', '', text1)  # website links handeled
            lis_txt = []
            for x1 in text1.split():
                lis_txt.append(''.join(e for e in x1 if e.isalnum()))
            text1 = ' '.join(lis_txt)
            text1 = re.sub(r"[^A-Za-z0-9]"," ",text1)
            #text1 = re.sub(r'[^ #|@[a-zA-Z]+',"",text1) # special characters removed
            # text1 = ' '.join(word.strip(string.punctuation) for word in "Hello, world. I'm a boy, you're a girl.".split())
            # #text1 = text1.split()
            #for i in range(len(text1)):
                # if (text1[i] == r'#|@[a-zA-Z]+\b'):  # regex needs to be modified
                #     print(text1[i])
                # else:
                #     text1[i] = re.sub(r'[!@#$%^&*,.()_\-<>?:;~`\[\]\(\)\\\/\"\']', "", text1[i])
                #     #re.sub(r'[^a-zA-Z0-9]', "", text1[i])
                    #re.sub(r'\'',"",text1[i])
                    #pass
            #text1 = re.sub(r'([A-Z])', r' \1', text1)  # Joint words splitted
            text1 = text1.lower()  # text case handeled
            text1 = re.sub(r"rt", " ", text1)
            text1 = re.sub(r"qt", " ", text1)
            text1 = re.sub(r"(http)\w*", " ", text1)  # http tags removed
            text1 = re.sub(r"\bamp\b", " ", text1)  # remaining tags handled
            text1 = re.sub(r"\s+", " ", text1)  # whitespaces handled
            #text1 = re.sub("\d+", "", text1)  # Removing digits
            text1 = ''.join(''.join(s)[:2] for _, s in itertools.groupby(text1))  # elongated/exagerated words handled
            text1 = ' '.join([word for word in text1.split() if word not in (stop)])  # stopwords removed
            # Processed text appended to empty list
        except(TypeError):  # exception command incase of type error
            text1 = text
        return text1
